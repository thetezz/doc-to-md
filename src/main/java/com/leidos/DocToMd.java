package com.leidos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DocToMd {

    public static void main (String[] args) throws Exception {
    	//final String PATTERN = "(/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/|[\\t]*//.*)|\"(\\\\.|[^\\\\\"])*\"|'(\\\\[\\s\\S]|[^'])*'";
    	//final String PATTERN = "(?s)/\\*(.*?)(?=\\*/)";
    	final String PATTERN = "/\\*([^*]|\\*[^/])*\\*/\\n.*";
    //	final String PATTERN = "/\\*\\*\\n\\ \\*.*";
    
        ArrayList<DocData> docList = new ArrayList<DocData>();
        
    	String filename = "./utils.groovy"; 
    	BufferedReader reader = new BufferedReader(new FileReader(filename));
        
    	String line = reader.readLine();
    	StringBuilder sb = new StringBuilder();
    	while (line != null) {
    		sb.append(line).append("\n");
    		line = reader.readLine();
    	}
    	String fileAsString = sb.toString();
    	reader.close();
    	Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(fileAsString.toString());
        while (matcher.find()) {
        	String string = matcher.group(0);
        	
        	//split on the annotation but ignore anything in curly brackets
        	
        	String [] items = string.split("[^{]@");
        	if (items[0] == null) 
        		throw(new Error());
        	
        //	String str = scrub(items[0]);
        	DocData dd = new DocData();
        	
        //	md.setMethodDescription(str);
    		
            for (int i=0; i < items.length; i++) {	
            //	System.out.println("ITEM " + i + "\n" + items[i]);
           // 	System.out.println("SCRUBBED ITEM " + i + "\n" + scrub(items[i]));
            	String item = scrub(items[i]);

            	if (i == items.length - 1) { //last item needs to be split
            		String [] itemsInLastToken = item.split("def ");
            		if (itemsInLastToken[1] == null) {
            			throw (new Error());
            		}
            		String str = itemsInLastToken[1].replaceFirst("\\{", "");
            		dd.setMethodDeclaration(str);
            		str = itemsInLastToken[0];
            		if(str.startsWith("return ")) {
            			str = str.replaceAll("return ", "");
            			dd.setReturnDescription(str);	
            		}
            		else if(str.startsWith("param ")) {
            			str = str.replaceAll("param ", "");
            			ArrayList<String> pd = dd.getParamDescriptions();
            			pd.add(str);
            		}
            		else { //last case will assume its the method description
            			dd.setMethodDescription(itemsInLastToken[0]);
            		}	      		
            	}
            	else {
            		if (item.startsWith("return ")) {
            			dd.setReturnDescription(item.replaceAll("return ",""));
            		}
            		else if(item.startsWith("param ")) {
            			String str = item.replaceAll("param ", "");
            			ArrayList<String> pd = dd.getParamDescriptions();
            			pd.add(str);
            		}
            		else {
            			dd.setMethodDescription(item);
            		}
            		
            	}
            }
            docList.add(dd);
        }
        MarkdownWriter mw = new MarkdownWriter();
        mw.setDocList(docList);
        
        mw.printFromDocData();
    }

    public static String scrub(String str) {
    	String returnStr;
    	returnStr = str.replaceAll("\\/\\*\\*\n \\* ", "");
    	returnStr = returnStr.replaceAll(" \\*", "");
    	returnStr = returnStr.replaceAll(" */", "");
    	returnStr = returnStr.replaceAll("\n", ""); 
    	return returnStr;	
    }
}
