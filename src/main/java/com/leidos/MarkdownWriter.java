package com.leidos;

import java.util.ArrayList;

public class MarkdownWriter {
	private ArrayList<DocData> docList = new ArrayList<DocData>();

	public ArrayList<DocData> getDocList() {
		return docList;
	}

	public void setDocList(ArrayList<DocData> docList) {
		this.docList = docList;
	}
	
	public void printFromDocData() throws RuntimeException {
		System.out.println("# Documentation");
        for (int i = 0; i < docList.size(); i++) {
        	System.out.println("## `" + docList.get(i).getMethodDeclaration() + "`");
        	System.out.println(docList.get(i).getMethodDescription());
        	for (int anint = 0; anint < docList.get(i).getParamDescriptions().size(); anint++) { 
        		String [] items = docList.get(i).getParamDescriptions().get(anint).split(" ", 2);
        		if (items.length != 2) {
        			throw new RuntimeException("format problem.");
        		}
        		System.out.println(" * **Parameter:** `" + items[0] + "` - " + items[1]);
        	}
        	System.out.println(" * **Return:** " + docList.get(i).getReturnDescription());

        }
	}
}
