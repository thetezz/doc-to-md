package com.leidos;

import java.util.ArrayList;

public class DocData {
	private String methodDescription = null;
	private ArrayList<String> paramDescriptions = new ArrayList<String>();
	private String returnDescription;
	private String methodDeclaration = null;
	
	public String getMethodDeclaration() {
		return methodDeclaration;
	}

	public void setMethodDeclaration(String methodDeclaration) {
		this.methodDeclaration = methodDeclaration;
	}

	public ArrayList<String> getParamDescriptions() {
		return paramDescriptions;
	}

	public void setParamDescriptions(ArrayList<String> paramDescriptions) {
		this.paramDescriptions = paramDescriptions;
	}

	public String getReturnDescription() {
		return returnDescription;
	}

	public void setReturnDescription(String returnDescription) {
		this.returnDescription = returnDescription;
	}

	public String getMethodDescription() {
		return methodDescription;
	}

	public void setMethodDescription(String description) {
		this.methodDescription = description;
	}
	
	

}
